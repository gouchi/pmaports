# Reference: <https://postmarketos.org/vendorkernel>
# Maintainer:  Clayton Craft <clayton@craftyguy.net>
pkgname=linux-purism-librem5
# Note: _p#, where # is the purism version, e.g. the '2' in 'librem5.2'
pkgver=5.8.0_p1
pkgrel=0
_kernver=${pkgver%_p*}
_purismrel=${pkgver#*_p}
# <kernel ver>.<purism kernel release>
_purismver=${_kernver}+librem5.$_purismrel
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://source.puri.sm/Librem5/linux-next"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev
	     devicepkg-dev bison flex openssl-dev"

# Source
_repository="linux-next"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"
source="
	$pkgname-$_purismver.tar.gz::https://source.puri.sm/Librem5/$_repository/-/archive/pureos/$_purismver/$_repository-pureos-$_purismver.tar.gz
	$_config
"
builddir="$srcdir/$_repository-pureos-$_purismver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS" \
		LOCALVERSION=".$_purismrel"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

}
sha512sums="e257f39a653b146d6e5d85df7bd657153ed540f002e52c7567e3545aa27d349e0adad3a0e7ebed22a819094033e45651968f8c7937f80231acd9e741570b4b79  linux-purism-librem5-5.8.0+librem5.1.tar.gz
13e6d08201b839318c9f0da8d26e5b6d1cecfcba82ff1a2cd5e589e92461b5685b89a802dddaa86dcd1f0d22b7d1a7b728150b7a0479bf3a8584b6562902be48  config-purism-librem5.aarch64"
