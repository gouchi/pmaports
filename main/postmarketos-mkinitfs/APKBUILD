# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
pkgname=postmarketos-mkinitfs
pkgver=0.14.1
pkgrel=1
pkgdesc="Tool to generate initramfs images for postmarketOS"
url="https://postmarketos.org"
depends="
	busybox-extras
	bzip2
	charging-sdl
	cmd:kpartx
	cryptsetup
	device-mapper
	e2fsprogs
	e2fsprogs-extra
	kmod
	lddtree
	lz4
	osk-sdl
	parted
	triggerhappy
	xz
	"
replaces="mkinitfs"
triggers="$pkgname.trigger=/etc/postmarketos-mkinitfs/hooks:/usr/share/kernel/*:/usr/share/postmarketos-mkinitfs-triggers"
source="00-default.modules
	init.sh.in
	init_functions.sh
	mkinitfs.sh
	mkinitfs_functions.sh
	mkinitfs_test.sh
	"
arch="noarch"
license="GPL-2.0-or-later"
provides="mkinitfs=0.0.1"

package() {
	for file in init.sh.in init_functions.sh mkinitfs_functions.sh; do
		install -Dm644 "$srcdir/$file" \
			"$pkgdir/usr/share/postmarketos-mkinitfs/$file"
	done

	install -Dm644 "$srcdir/00-default.modules" \
		"$pkgdir/etc/postmarketos-mkinitfs/modules/00-default.modules"

	install -Dm755 "$srcdir/mkinitfs.sh" \
		"$pkgdir/sbin/mkinitfs"

	mkdir -p "$pkgdir/etc/postmarketos-mkinitfs/hooks/"
}

check() {
	/bin/busybox sh ./mkinitfs_test.sh
}

sha512sums="5037cb7285bb7c0c40ca9e6df332d882ef9a8b379756c785f921e062dab1b7e7f3139d00897f69323a916d709ced4297fea8cbd3a13ebae575b873ec9e2cbfae  00-default.modules
7fc6ba96419628850984df3322b73bcf655cba03fe3e97ec7425e66e90f4b2a4de106b5cb2e3d46785a92557334288011334104285e2050c7a8e823b8fa5668c  init.sh.in
e9bbf6850c0c44f10c8fe66ceb05ed454844646dd937b5fe614810a0cce7c7383febe5ee1b0830217bff71a29621d82aa57adbcd59cb24a72701504887513ff1  init_functions.sh
dfc01ee0547ea88b7aa45a005e842b636e9e19bbf1705f3dad53a66d57af7c5c513c092b5469a06d9b00322e56a4d25f1b47e4c5324aafa99f5291679968d1f1  mkinitfs.sh
6f2948e5653076cc5e987a0438b62c0492fbbda911dce8153628bd749987ecbce3bf2f95594a49beda1b9ff851aa368c550f4ea7830cbf6e97784b4289183493  mkinitfs_functions.sh
c7a3c33daeb12b33ac72207191941c4d634f15c22958273b52af381a70ebaba1d3a9299483f0c447d9e66c560151fe7b9588bb4bbef2c8914f83185984ee4622  mkinitfs_test.sh"
